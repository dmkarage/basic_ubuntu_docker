# Use an official ROS image
FROM ubuntu:18.04

SHELL ["/bin/bash", "-c"]

ENV CONTAINER_USER="user"

WORKDIR /home/$CONTAINER_USER

RUN pwd

RUN apt-get update && apt-get install -y terminator nano gedit nmap 